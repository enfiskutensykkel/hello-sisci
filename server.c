#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "sisci_api.h"
#include "common.h"

static sci_error_t init_local_segment(const dev_handle_t* dev, uint key_offset, uint seg_size, loc_seg_handle_t* seg)
{
	sci_error_t err;
	
	seg->key = SEGMENT_KEY_LOCAL(dev, key_offset); 
	seg->size = seg_size;

	SCICreateSegment(dev->device, &seg->segment, seg->key, seg->size, NULL, NULL, 0, &err);
	if (err != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to create segment: %x\n", err);
		return err;
	}

	SCIPrepareSegment(seg->segment, dev->adapter_no, 0, &err);
	if (err != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to prepare segment: %x\n", err);
		return err;
	}

	return SCI_ERR_OK;
}

static sci_error_t map_local_segment(const loc_seg_handle_t* seg, uint offset, map_handle_t* map)
{
	sci_error_t err;

	map->addr = SCIMapLocalSegment(seg->segment, &map->map, offset, seg->size, NULL, 0, &err);
	if (err != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to map segment to memory area: %x\n", err);
		return err;
	}

	return SCI_ERR_OK;
}

static sci_error_t destroy_local_segment(const loc_seg_handle_t* seg, const map_handle_t* map)
{
	sci_error_t err;

	SCIUnmapSegment(map->map, 0, &err);
	if (err != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to unmap segment: %x\n", err);
		return err;
	}

	SCIRemoveSegment(seg->segment, 0, &err);
	if (err != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to remove segment: %x\n", err);
		return err;
	}
	
	return SCI_ERR_OK;
}

static inline int magic_signature(sync_msg_t* msg)
{
	return msg->data[0] == 0xde
		&& msg->data[1] == 0xad
		&& msg->data[2] == 0xbe
		&& msg->data[3] == 0xef;
}

sci_error_t server_node(const dev_handle_t* dev)
{
	sci_error_t err;

	// Prepare segment
	loc_seg_handle_t seg;
	err = init_local_segment(dev, 0, sizeof(sync_msg_t), &seg);
	if (err != SCI_ERR_OK)
	{
		return err;
	}
	
	// Map segment to userland memory
	map_handle_t map;
	err = map_local_segment(&seg, 0, &map);
	if (err != SCI_ERR_OK)
	{
		return err;
	}

	SCISetSegmentAvailable(seg.segment, dev->adapter_no, 0, &err);
	if (err != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to set segment available: %x\n", err);
		return err;
	}

	fprintf(stdout, "Waiting for messages...\n");
	sync_msg_t *new = (sync_msg_t*) map.addr;
	sync_msg_t old = *new;

	while (true)
	{
		if (magic_signature(new))
		{
			fprintf(stdout, "Received terminate\n");
			break;
		}

		if (memcmp(&old, new, sizeof(sync_msg_t)) != 0)
		{
			old = *new;
			old.data[sizeof(old.data) - 1] = '\0';
			fprintf(stdout, "Got new message: %s\n", (char*) old.data);
		}
	} 

	SCISetSegmentUnavailable(seg.segment, dev->adapter_no, 0, &err);
	if (err != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to make segment unavailable: %x\n", err);
		return err;
	}

	err = destroy_local_segment(&seg, &map);
	if (err != SCI_ERR_OK)
	{
		return err;
	}

	return SCI_ERR_OK;
}
