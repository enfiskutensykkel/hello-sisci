#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdbool.h>
#include "sisci_api.h"
#include "common.h"

#define DEFAULT_ADAPTER_NO 0
#define NO_NODE_ID -1

extern sci_error_t server_node(const dev_handle_t* device_handle);
extern sci_error_t client_node(const dev_handle_t* device_handle);

void print_dev(dev_handle_t* device)
{
	fprintf(stdout, "Local adapter number: %u\n", device->adapter_no);
	fprintf(stdout, "Local node ID: %u\n", device->local_node_id);
	fprintf(stdout, "Remote node ID: %u\n", device->remote_node_id);
}

static void give_usage(const char* prog_name, int exit_code)
{
	FILE* fp = exit_code == 0 ? stdout : stderr;
	fprintf(fp, 
			"Usage: %s -r <remote node id> [-c] [-a <local adapter no>]\n\n"
			"  -r\tremote node ID\n"
			"  -c\trun as client (if not given, run as server)\n"
			"  -a\tlocal adapter number (if not given, %u is used as default)\n"
			, prog_name, DEFAULT_ADAPTER_NO);
	fprintf(stderr, "\nBuild time %s, %s\n", __DATE__, __TIME__);
	exit(exit_code);
}

int main(int argc, char** argv)
{
	uint local_adapter_no = DEFAULT_ADAPTER_NO;
	uint remote_node_id = NO_NODE_ID;
	uint local_node_id = NO_NODE_ID;
	bool client = false;
	
	// Helper variables to process options
	char* strptr = NULL;
	int opt, optidx = -1;

	// Parse program options
	while ((opt = getopt_long(argc, argv, ":hcr:a:", NULL, &optidx)) != -1)
	{
		switch (opt)
		{
			case ':': // missing value
				fprintf(stderr, "Option -%c requires a value\n", optopt);
				give_usage(argv[0], 1);
				break;

			case '?': // unknown flag
				fprintf(stderr, "Unknown option: -%c\n", optopt);
				give_usage(argv[0], 1);
				break;

			case 'h':
				give_usage(argv[0], 0);
				break;

			case 'c':
				client = true;
				break;

			case 'r':
				strptr = NULL;
				remote_node_id = strtoul(optarg, &strptr, 0);
				if (strptr == NULL || *strptr != '\0')
				{
					fprintf(stderr, "Option -r requires a numeric argument\n");
					give_usage(argv[0], 1);
				}
				break;

			case 'a':
				strptr = NULL;
				local_adapter_no = strtoul(optarg, &strptr, 0);
				if (strptr == NULL || *strptr != '\0')
				{
					fprintf(stderr, "Option -a requires a numeric argument\n");
					give_usage(argv[0], 1);
				}
				break;
		}
	}

	if (remote_node_id == NO_NODE_ID)
	{
		fprintf(stderr, "No remote node ID given\n");
		give_usage(argv[0], 1);
	}

	sci_error_t error;
	sci_desc_t sd;

	SCIInitialize(0, &error);
	if (error != SCI_ERR_OK)
	{
		fprintf(stderr, "Initialization of SISCI failed, are you root?\n");
		exit(1);
	}

	SCIOpen(&sd, 0, &error);
	if (error != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to open device descriptor\n");
		if (error == SCI_ERR_INCONSISTENT_VERSIONS)
		{
			fprintf(stderr, "Version mismatch between user-space library and SISCI driver\n");
		}
		SCITerminate();
		exit(1);
	}

	SCIGetLocalNodeId(local_adapter_no, &local_node_id, 0, &error);
	if (error != SCI_ERR_OK)
	{
		fprintf(stderr, "Could not find the local adapter %u\n", local_adapter_no);
		SCIClose(sd, 0, &error);
		SCITerminate();
		exit(1);
	}

	dev_handle_t device = 
	{
		.device = sd,
		.adapter_no = local_adapter_no,
		.local_node_id = local_node_id,
		.remote_node_id = remote_node_id
	};

	print_dev(&device);

	if (client)
	{
		error = client_node(&device);
	}
	else
	{
		error = server_node(&device);
	}

	if (error != SCI_ERR_OK)
	{
		fprintf(stderr, "Node failed\n");
		SCIClose(sd, 0, &error);
		SCITerminate();
		exit(1);
	}

	SCIClose(sd, 0, &error);
	if (error != SCI_ERR_OK)
	{
		fprintf(stderr, "Unknown error on close\n");
	}

	SCITerminate();
	exit(0);
}

