#ifndef __SISCI_COMMON_H__
#define __SISCI_COMMON_H__

#include "sisci_api.h"

typedef unsigned int uint;
typedef unsigned char byte;

typedef struct {
	sci_desc_t device;
	uint adapter_no;
	uint local_node_id;
	uint remote_node_id;
} dev_handle_t;

typedef struct {
	uint key;
	uint size;
	sci_local_segment_t segment;
} loc_seg_handle_t;

typedef struct {
	uint key;
	uint size;
	sci_remote_segment_t segment;
} rem_seg_handle_t;


typedef struct {
	sci_map_t map;
	volatile void* addr;
} map_handle_t;

typedef struct {
	byte data[32];
} sync_msg_t;

#define SEGMENT_KEY_LOCAL(dev_handle, offset) ( ((dev_handle)->local_node_id << 16) | (((dev_handle)->remote_node_id + 1) << 8) | (offset) )
#define SEGMENT_KEY_REMOTE(dev_handle, offset) ( ((dev_handle)->remote_node_id << 16) | (((dev_handle)->local_node_id + 1) << 8) | (offset) )

#endif
