#include <stdio.h>
#include <string.h>
#include "sisci_api.h"
#include "common.h"

static sci_error_t init_remote_segment(dev_handle_t* dev, uint key_offset, uint seg_size, rem_seg_handle_t* seg)
{
	sci_error_t err;

	seg->key = SEGMENT_KEY_REMOTE(dev, key_offset); 
	seg->size = seg_size;

	fprintf(stdout, "Connecting to remote node...\n");
	do
	{
		SCIConnectSegment(dev->device, &seg->segment, dev->remote_node_id, seg->key, dev->adapter_no, NULL, NULL, SCI_INFINITE_TIMEOUT, 0, &err);
	}
	while (err != SCI_ERR_OK);

	return SCI_ERR_OK;
}

static sci_error_t map_remote_segment(rem_seg_handle_t* seg, uint offset, map_handle_t* map)
{
	sci_error_t err;

	map->addr = SCIMapRemoteSegment(seg->segment, &map->map, offset, seg->size, NULL, 0, &err);
	if (err != SCI_ERR_OK)
	{
		fprintf(stderr, "Mapping remote segment to local memory failed: %x\n", err);
		return err;
	}

	return SCI_ERR_OK;
}

static sci_error_t destroy_remote_segment(rem_seg_handle_t* seg, map_handle_t* map)
{
	sci_error_t err;

	SCIUnmapSegment(map->map, 0, &err);
	if (err != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to unmap remote segment: %x\n", err);
		return err;
	}

	SCIDisconnectSegment(seg->segment, 0, &err);
	if (err != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to disconnect remote segment: %x\n", err);
		return err;
	}

	return SCI_ERR_OK;
}

static sci_error_t send_sync_msg(map_handle_t* map, const sync_msg_t* msg)
{
	sci_error_t error;
	sci_sequence_t sequence;
	sci_sequence_status_t sequence_status;

	SCICreateMapSequence(map->map, &sequence, 0, &error);
	if (error != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to initialize map sequence: %x\n", error);
		return error;
	}

	do
	{
		sequence_status = SCIStartSequence(sequence, 0, &error);
	}
	while (sequence_status != SCI_SEQ_OK);

	volatile sync_msg_t* ptr = (sync_msg_t*) map->addr;
	*ptr = *msg;

	sequence_status = SCICheckSequence(sequence, 0, &error);
	if (sequence_status != SCI_SEQ_OK)
	{
		fprintf(stderr, "Sync transfer failed\n");
		return SCI_ERR_TRANSFER_FAILED;
	}

	SCIRemoveSequence(sequence, 0, &error);
	if (error != SCI_ERR_OK)
	{
		fprintf(stderr, "Failed to remove map sequence: %x\n", error);
		return error;
	}

	return SCI_ERR_OK;
}

static sci_error_t send_data(map_handle_t* map)
{
	sync_msg_t message;
	const char* string = "Hello, world!";
	uint n = strlen(string);

	strncpy((char*) message.data, string, (n > sizeof(message.data) ? sizeof(message.data) : n)); 

	return send_sync_msg(map, &message);
}

static sci_error_t send_terminate(map_handle_t* map)
{
	sync_msg_t message;
	message.data[0] = 0xde;
	message.data[1] = 0xad;
	message.data[2] = 0xbe;
	message.data[3] = 0xef;

	return send_sync_msg(map, &message);
}

sci_error_t client_node(dev_handle_t* dev)
{
	sci_error_t err;

	if (!SCIProbeNode(dev->device, dev->adapter_no, dev->remote_node_id, 0, &err))
	{
		fprintf(stderr, "Could not reach remote node %u\n", dev->remote_node_id);
		return err;
	}

	rem_seg_handle_t segment;
	err = init_remote_segment(dev, 0, sizeof(sync_msg_t), &segment);
	if (err != SCI_ERR_OK)
	{
		return err;
	}

	map_handle_t mapping;
	err = map_remote_segment(&segment, 0, &mapping);
	if (err != SCI_ERR_OK)
	{
		return err;
	}

	fprintf(stdout, "Sending data...\n");
	send_data(&mapping);

	fprintf(stdout, "Terminating...\n");
	err = send_terminate(&mapping);
	if (err != SCI_ERR_OK)
	{
		return err;
	}

	err = destroy_remote_segment(&segment, &mapping);
	if (err != SCI_ERR_OK)
	{
		return err;
	}

	return SCI_ERR_OK;
}
