IPATH := /opt/DIS/include /opt/DIS/include/dis
CFLAGS := -std=gnu99 -Wall -pedantic

SRC_FILES := $(shell find . -type f -name "*.c")

LBITS := $(shell getconf LONG_BIT)
ifeq ($(LBITS),64)
LPATH := /opt/DIS/lib64
else
LPATH := /opt/DIS/lib
endif

ifneq ($(USER),root)
$(error You must be root)
endif

.PHONY: all clean hello

all: hello

hello: $(SRC_FILES:%.c=%.o)
	$(CC) -o $@ $^ -lsisci -L$(LPATH) 

clean:
	rm -f $(SRC_FILES:%.c=%.o) hello

%.o: %.c
	$(CC) $(CFLAGS) $(addprefix -I,$(IPATH)) -DD_REENTRANT -o $@ $< -c
